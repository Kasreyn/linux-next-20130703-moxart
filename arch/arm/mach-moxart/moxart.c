/*
 * arch/arm/mach-moxart/moxart.c
 *
 * (C) Copyright 2013, Jonas Jensen <jonas.jensen@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <linux/init.h>
#include <linux/clocksource.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>
#include <linux/clk-provider.h>

#include <asm/mach/arch.h>
#include <asm/system_misc.h>

#define REG_COUNT	0x4
#define REG_MODE	0x8
#define REG_ENABLE	0xC

static void __iomem *wdt_base;

static void moxart_wdt_restart(enum reboot_mode reboot_mode, const char *cmd)
{
	writel(1, wdt_base + REG_COUNT);
	writel(0x5ab9, wdt_base + REG_MODE);
	writel(0x03, wdt_base + REG_ENABLE);
}

static const struct of_device_id moxart_watchdog_match[] = {
	{ .compatible = "moxa,moxart-watchdog", .data = moxart_wdt_restart },
	{ },
};

static void moxart_setup_restart(void)
{
	const struct of_device_id *of_id;
	struct device_node *np;

	np = of_find_matching_node(NULL, moxart_watchdog_match);
	if (WARN(!np, "unable to setup watchdog restart"))
		return;

	wdt_base = of_iomap(np, 0);
	WARN(!wdt_base, "failed to map watchdog base address");

	of_id = of_match_node(moxart_watchdog_match, np);
	WARN(!of_id, "restart function not available");

	arm_pm_restart = of_id->data;
}

static void __init moxart_init_time(void)
{
	of_clk_init(NULL);
	clocksource_of_init();
}

static void __init moxart_dt_init(void)
{
	moxart_setup_restart();
	of_platform_populate(NULL, of_default_bus_match_table, NULL, NULL);
}

static const char * const moxart_dt_compat[] = {
	"moxa,moxart",
	NULL,
};

DT_MACHINE_START(MOXART, "MOXA UC-7112-LX")
	.init_machine		= moxart_dt_init,
	.init_time		= moxart_init_time,
	.dt_compat		= moxart_dt_compat,
MACHINE_END
