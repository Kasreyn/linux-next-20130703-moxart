/* MOXA ART Ethernet (RTL8201CP) driver.
 *
 * Copyright (C) 2013 Jonas Jensen
 *
 * Jonas Jensen <jonas.jensen@gmail.com>
 *
 * Based on code from
 * Moxa Technology Co., Ltd. <www.moxa.com>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/dma-mapping.h>
#include <linux/ethtool.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/crc32.h>
#include <linux/crc32c.h>

#include "moxart_ether.h"

static inline unsigned long moxart_emac_read(struct net_device *ndev,
					     unsigned int reg)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	return readl(priv->base + reg);
}

static inline void moxart_emac_write(struct net_device *ndev,
				     unsigned int reg, unsigned long value)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	writel(value, priv->base + reg);
}

static void moxart_update_mac_address(struct net_device *ndev)
{
	moxart_emac_write(ndev, MAC_MADR_REG_OFFSET,
			  ((ndev->dev_addr[0] << 8) | (ndev->dev_addr[1])));
	moxart_emac_write(ndev, MAC_MADR_REG_OFFSET + 4,
			  ((ndev->dev_addr[2] << 24) |
			   (ndev->dev_addr[3] << 16) |
			   (ndev->dev_addr[4] << 8) |
			   (ndev->dev_addr[5])));
}

static int moxart_set_mac_address(struct net_device *ndev, void *addr)
{
	struct sockaddr *address = addr;

	if (!is_valid_ether_addr(address->sa_data))
		return -EADDRNOTAVAIL;


	memcpy(ndev->dev_addr, address->sa_data, ndev->addr_len);
	moxart_update_mac_address(ndev);

	return 0;
}

static void moxart_mac_free_memory(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	if (priv->virt_tx_desc_baseaddr)
		dma_free_coherent(NULL, sizeof(struct tx_desc_t)*TX_DESC_NUM,
				  priv->virt_tx_desc_baseaddr,
				  priv->phy_tx_desc_baseaddr);
	if (priv->virt_rx_desc_baseaddr)
		dma_free_coherent(NULL, sizeof(struct rx_desc_t)*RX_DESC_NUM,
				  priv->virt_rx_desc_baseaddr,
				  priv->phy_rx_desc_baseaddr);
	if (priv->virt_tx_buf_baseaddr)
		dma_free_coherent(NULL, TX_BUF_SIZE*TX_DESC_NUM,
				  priv->virt_tx_buf_baseaddr,
				  priv->phy_tx_buf_baseaddr);
	if (priv->virt_rx_buf_baseaddr)
		dma_free_coherent(NULL, RX_BUF_SIZE*RX_DESC_NUM,
				  priv->virt_rx_buf_baseaddr,
				  priv->phy_rx_buf_baseaddr);
}

static void moxart_mac_reset(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	writel(SW_RST, priv->base + MACCR_REG_OFFSET);
	while (readl(priv->base + MACCR_REG_OFFSET) & SW_RST)
		mdelay(10);

	writel(0, priv->base + IMR_REG_OFFSET);

	priv->maccr = RX_BROADPKT | FULLDUP | CRC_APD | RX_FTL;
}

static void moxart_mac_enable(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	writel(0x00001010, priv->base + ITC_REG_OFFSET);
	writel(0x00000001, priv->base + APTC_REG_OFFSET);
	writel(0x00000390, priv->base + DBLAC_REG_OFFSET);

	writel(RPKT_FINISH_M, priv->base + IMR_REG_OFFSET);
	priv->maccr |= (RCV_EN | XMT_EN | RDMA_EN | XDMA_EN);
	writel(priv->maccr, priv->base + MACCR_REG_OFFSET);
}

static void moxart_mac_setup_desc_ring(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);
	struct tx_desc_t *txdesc;
	struct rx_desc_t *rxdesc;
	unsigned char *virtbuf;
	unsigned int phybuf;
	int i;

	virtbuf = priv->virt_tx_buf_baseaddr;
	phybuf = priv->phy_tx_buf_baseaddr;
	for (i = 0; i < TX_DESC_NUM; i++,
		virtbuf += TX_BUF_SIZE, phybuf += TX_BUF_SIZE) {
		txdesc = &priv->virt_tx_desc_baseaddr[i];
		memset(txdesc, 0, sizeof(struct tx_desc_t));
		txdesc->txdes2.phy_tx_buf_baseaddr = phybuf;
		txdesc->txdes2.virt_tx_buf_baseaddr = virtbuf;
	}
	priv->virt_tx_desc_baseaddr[TX_DESC_NUM - 1].txdes1.ubit.edotr = 1;

	virtbuf = priv->virt_rx_buf_baseaddr;
	phybuf = priv->phy_rx_buf_baseaddr;
	for (i = 0; i < RX_DESC_NUM; i++,
		virtbuf += RX_BUF_SIZE, phybuf += RX_BUF_SIZE) {
		rxdesc = &priv->virt_rx_desc_baseaddr[i];
		memset(rxdesc, 0, sizeof(struct rx_desc_t));
		rxdesc->rxdes0.ubit.rx_dma_own = 1;
		rxdesc->rxdes1.ubit.rx_buf_size = RX_BUF_SIZE;
		rxdesc->rxdes2.phy_rx_buf_baseaddr = phybuf;
		rxdesc->rxdes2.virt_rx_buf_baseaddr = virtbuf;
	}
	priv->virt_rx_desc_baseaddr[RX_DESC_NUM - 1].rxdes1.ubit.edorr = 1;
	priv->tx_desc_now = priv->rx_desc_now = 0;

	/* reset the MAC controler TX/RX desciptor base address */
	writel(priv->phy_tx_desc_baseaddr, priv->base + TXR_BADR_REG_OFFSET);
	writel(priv->phy_rx_desc_baseaddr, priv->base + RXR_BADR_REG_OFFSET);
}

static int moxart_mac_open(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	if (!is_valid_ether_addr(ndev->dev_addr))
		return -EADDRNOTAVAIL;

	spin_lock_irq(&priv->txlock);
	moxart_mac_reset(ndev);
	moxart_update_mac_address(ndev);
	moxart_mac_setup_desc_ring(ndev);
	moxart_mac_enable(ndev);
	spin_unlock_irq(&priv->txlock);
	netif_start_queue(ndev);

	netdev_dbg(ndev, "%s: IMR=0x%x, MACCR=0x%x\n",
		   __func__, readl(priv->base + IMR_REG_OFFSET),
		   readl(priv->base + MACCR_REG_OFFSET));
	return 0;
}

static int moxart_mac_stop(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	netif_stop_queue(ndev);
	spin_lock_irq(&priv->txlock);

	/* disable all interrupts */
	writel(0, priv->base + IMR_REG_OFFSET);

	/* disable all functions */
	writel(0, priv->base + MACCR_REG_OFFSET);

	spin_unlock_irq(&priv->txlock);

	return 0;
}

static void moxart_mac_recv(struct work_struct *ptr)
{
	struct net_device *ndev = (struct net_device *) ptr;
	struct moxart_mac_priv_t *priv = netdev_priv((struct net_device *)ptr);
	struct rx_desc_t *rxdesc;
	struct sk_buff *skb;
	unsigned int ui, len;
	int rxnow = priv->rx_desc_now;
	int loops = RX_DESC_NUM;

repeat_recv:
	rxdesc = &priv->virt_rx_desc_baseaddr[rxnow];
	ui = rxdesc->rxdes0.ui;

	if (ui & RXDMA_OWN)
		return;

	if (ui & (RX_ERR | CRC_ERR | FTL | RUNT | RX_ODD_NB)) {
		netdev_err(ndev, "%s: packet error\n", __func__);
		priv->stats.rx_dropped++;
		priv->stats.rx_errors++;
		goto recv_finish;
	}

	len = ui & RFL_MASK;

	if (len > RX_BUF_SIZE)
		len = RX_BUF_SIZE;

	skb = dev_alloc_skb(len + 2);
	if (skb == NULL) {
		netdev_err(ndev, "%s: dev_alloc_skb failed\n", __func__);
		priv->stats.rx_dropped++;
		goto recv_finish;
	}
	skb_reserve(skb, 2);
	skb->dev = ndev;

	memcpy(skb_put(skb, len), rxdesc->rxdes2.virt_rx_buf_baseaddr, len);
	netif_rx(skb);

	skb->protocol = eth_type_trans(skb, ndev);
	ndev->last_rx = jiffies;
	priv->stats.rx_packets++;
	priv->stats.rx_bytes += len;
	if (ui & MULTICAST_RXDES0)
		priv->stats.multicast++;

recv_finish:
	rxdesc->rxdes0.ui = RXDMA_OWN;
	rxnow++;
	rxnow &= RX_DESC_NUM_MASK;
	priv->rx_desc_now = rxnow;
	if (loops-- > 0)
		goto repeat_recv;
}

static irqreturn_t moxart_mac_interrupt(int irq, void *dev_id)
{
	struct net_device *ndev = (struct net_device *) dev_id;
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);
	unsigned int ists = readl(priv->base + ISR_REG_OFFSET);

	if (ists & RPKT_FINISH)
		moxart_mac_recv((void *) ndev);

	return IRQ_HANDLED;
}

static int moxart_mac_start_xmit(struct sk_buff *skb, struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);
	struct tx_desc_t *txdesc;
	int len;
	int txnow = priv->tx_desc_now;

	spin_lock_irq(&priv->txlock);
	txdesc = &priv->virt_tx_desc_baseaddr[txnow];
	if (txdesc->txdes0.ubit.tx_dma_own) {
		netdev_err(ndev, "%s: no TX space for packet\n", __func__);
		priv->stats.tx_dropped++;
		goto xmit_final;
	}

	len = skb->len > TX_BUF_SIZE ? TX_BUF_SIZE : skb->len;
	memcpy(txdesc->txdes2.virt_tx_buf_baseaddr, skb->data, len);

	if (skb->len < ETH_ZLEN) {
		memset(&txdesc->txdes2.virt_tx_buf_baseaddr[skb->len],
		       0, ETH_ZLEN - skb->len);
		len = ETH_ZLEN;
	}

	txdesc->txdes1.ubit.lts = 1;
	txdesc->txdes1.ubit.fts = 1;
	txdesc->txdes1.ubit.tx2_fic = 0;
	txdesc->txdes1.ubit.tx_ic = 0;
	txdesc->txdes1.ubit.tx_buf_size = len;
	txdesc->txdes0.ui = TXDMA_OWN;

	/* start to send packet */
	writel(0xffffffff, priv->base + TXPD_REG_OFFSET);

	txnow++;
	txnow &= TX_DESC_NUM_MASK;
	priv->tx_desc_now = txnow;
	ndev->trans_start = jiffies;
	priv->stats.tx_packets++;
	priv->stats.tx_bytes += len;

xmit_final:
	spin_unlock_irq(&priv->txlock);
	dev_kfree_skb_any(skb);

	return NETDEV_TX_OK;
}

static struct net_device_stats *moxart_mac_get_stats(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);
	int desc = priv->rx_desc_now;

	desc++;
	desc &= RX_DESC_NUM_MASK;
	return &priv->stats;
}

static void moxart_mac_setmulticast(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);
	struct netdev_hw_addr *ha;
	int crc_val;

	netdev_for_each_mc_addr(ha, ndev) {
		crc_val = crc32_le(~0, ha->addr, ETH_ALEN);
		crc_val = (crc_val >> 26) & 0x3f;
		if (crc_val >= 32) {
			writel(readl(priv->base + MATH1_REG_OFFSET) |
			       (1UL << (crc_val - 32)),
			       priv->base + MATH1_REG_OFFSET);
		} else {
			writel(readl(priv->base + MATH0_REG_OFFSET) |
			       (1UL << crc_val),
			       priv->base + MATH0_REG_OFFSET);
		}
	}
}

static void moxart_mac_set_rx_mode(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);

	spin_lock_irq(&priv->txlock);

	(ndev->flags & IFF_PROMISC) ? (priv->maccr |= RCV_ALL) :
				      (priv->maccr &= ~RCV_ALL);

	(ndev->flags & IFF_ALLMULTI) ? (priv->maccr |= RX_MULTIPKT) :
				       (priv->maccr &= ~RX_MULTIPKT);

	if ((ndev->flags & IFF_MULTICAST) && netdev_mc_count(ndev)) {
		priv->maccr |= HT_MULTI_EN;
		moxart_mac_setmulticast(ndev);
	} else {
		priv->maccr &= ~HT_MULTI_EN;
	}

	writel(priv->maccr, priv->base + MACCR_REG_OFFSET);

	spin_unlock_irq(&priv->txlock);
}

static struct net_device_ops moxart_netdev_ops = {
	.ndo_open		= moxart_mac_open,
	.ndo_stop		= moxart_mac_stop,
	.ndo_start_xmit		= moxart_mac_start_xmit,
	.ndo_get_stats		= moxart_mac_get_stats,
	.ndo_set_rx_mode	= moxart_mac_set_rx_mode,
	.ndo_set_mac_address	= moxart_set_mac_address,
	.ndo_validate_addr	= eth_validate_addr,
	.ndo_change_mtu		= eth_change_mtu,
};

static void moxart_get_mac_address(struct net_device *ndev)
{
	struct moxart_mac_priv_t *priv = netdev_priv(ndev);
	int i;

	for (i = 0; i <= 5; i++)
		ndev->dev_addr[i] = readb(priv->flash_base + i);
}

static int moxart_mac_probe(struct platform_device *pdev)
{
	struct device *p_dev = &pdev->dev;
	struct device_node *node = p_dev->of_node;
	struct net_device *ndev;
	struct moxart_mac_priv_t *priv;
	struct resource *res;
	unsigned int irq;

	ndev = alloc_etherdev(sizeof(struct moxart_mac_priv_t));
	if (!ndev)
		return -ENOMEM;

	irq = irq_of_parse_and_map(node, 0);

	priv = netdev_priv(ndev);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	ndev->base_addr = res->start;
	priv->base = devm_ioremap_resource(p_dev, res);
	if (IS_ERR(priv->base)) {
		dev_err(p_dev, "%s: devm_ioremap_resource res_mac failed\n",
			__func__);
		goto init_fail;
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);

	/* use ioremap here instead of devm_ioremap_resource because
	 * physmap_of requests the memory region first, doing it here
	 * again would fail
	 *
	 * if flash partition 0 (bootloader) was omitted from DT then
	 * this driver could request the region exclusively
	 */
	priv->flash_base = ioremap(res->start, resource_size(res));
	if (IS_ERR(priv->flash_base)) {
		dev_err(p_dev, "%s: devm_ioremap_resource res_flash failed\n",
			__func__);
		goto init_fail;
	}

	spin_lock_init(&priv->txlock);
	priv->virt_tx_desc_baseaddr = (struct tx_desc_t *)
		dma_alloc_coherent(NULL, sizeof(struct tx_desc_t) * TX_DESC_NUM,
		(dma_addr_t *)&priv->phy_tx_desc_baseaddr,
		GFP_DMA | GFP_KERNEL);
	if (priv->virt_tx_desc_baseaddr == NULL ||
		(priv->phy_tx_desc_baseaddr & 0x0f)) {
		netdev_err(ndev, "TX descriptor alloc failed\n");
		goto init_fail;
	}
	priv->virt_rx_desc_baseaddr = (struct rx_desc_t *)
		dma_alloc_coherent(NULL, sizeof(struct rx_desc_t) * RX_DESC_NUM,
		(dma_addr_t *)&priv->phy_rx_desc_baseaddr,
		GFP_DMA | GFP_KERNEL);
	if (priv->virt_rx_desc_baseaddr == NULL ||
		(priv->phy_rx_desc_baseaddr & 0x0f)) {
		netdev_err(ndev, "RX descriptor alloc failed\n");
		goto init_fail;
	}
	priv->virt_tx_buf_baseaddr = (unsigned char *)
		dma_alloc_coherent(NULL, TX_BUF_SIZE * TX_DESC_NUM,
		(dma_addr_t *)&priv->phy_tx_buf_baseaddr, GFP_DMA | GFP_KERNEL);
	if (priv->virt_tx_buf_baseaddr == NULL ||
		(priv->phy_tx_buf_baseaddr & 0x03)) {
		netdev_err(ndev, "TX buffer alloc failed\n");
		goto init_fail;
	}
	priv->virt_rx_buf_baseaddr = (unsigned char *)
		dma_alloc_coherent(NULL, RX_BUF_SIZE * RX_DESC_NUM,
		(dma_addr_t *)&priv->phy_rx_buf_baseaddr, GFP_DMA | GFP_KERNEL);
	if (priv->virt_rx_buf_baseaddr == NULL ||
		(priv->phy_rx_buf_baseaddr & 0x03)) {
		netdev_err(ndev, "RX buffer alloc failed\n");
		goto init_fail;
	}
	platform_set_drvdata(pdev, ndev);

	ether_setup(ndev);
	ndev->netdev_ops = &moxart_netdev_ops;
	ndev->priv_flags |= IFF_UNICAST_FLT;

	SET_NETDEV_DEV(ndev, &pdev->dev);

	moxart_get_mac_address(ndev);
	moxart_update_mac_address(ndev);

	if (register_netdev(ndev)) {
		free_netdev(ndev);
		goto init_fail;
	}

	ndev->irq = irq;

	if (devm_request_irq(p_dev, irq, moxart_mac_interrupt,
		IRQF_DISABLED, pdev->name, ndev)) {
		netdev_err(ndev, "%s: devm_request_irq failed\n", __func__);
		free_netdev(ndev);
		return -EBUSY;
	}

	netdev_dbg(ndev, "%s: IRQ=%d address=%02x:%02x:%02x:%02x:%02x:%02x\n",
		    __func__, ndev->irq,
		    ndev->dev_addr[0], ndev->dev_addr[1], ndev->dev_addr[2],
		    ndev->dev_addr[3], ndev->dev_addr[4], ndev->dev_addr[5]);
	return 0;

init_fail:
	netdev_err(ndev, "%s: init failed\n", __func__);
	moxart_mac_free_memory(ndev);
	return -ENOMEM;
}

static int moxart_remove(struct platform_device *pdev)
{
	struct net_device *ndev = platform_get_drvdata(pdev);
	unregister_netdev(ndev);
	free_irq(ndev->irq, ndev);
	moxart_mac_free_memory(ndev);
	platform_set_drvdata(pdev, NULL);
	free_netdev(ndev);
	return 0;
}

static const struct of_device_id moxart_mac_match[] = {
	{ .compatible = "moxa,moxart-mac" },
	{ }
};

struct __initdata platform_driver moxart_mac_driver = {
	.probe	= moxart_mac_probe,
	.remove	= moxart_remove,
	.driver	= {
		.name		= "moxart-ethernet",
		.owner		= THIS_MODULE,
		.of_match_table	= moxart_mac_match,
	},
};
module_platform_driver(moxart_mac_driver);

MODULE_DESCRIPTION("MOXART RTL8201CP Ethernet driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonas Jensen <jonas.jensen@gmail.com>");

