/*
 * MOXA ART SoCs watchdog driver.
 *
 * Copyright (C) 2013 Jonas Jensen
 *
 * Jonas Jensen <jonas.jensen@gmail.com>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/clk.h>
#include <linux/err.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>
#include <linux/types.h>
#include <linux/watchdog.h>

#include <asm/system_misc.h>

#define REG_COUNT			0x4
#define REG_MODE			0x8
#define REG_ENABLE			0xC

struct moxart_wdt_dev {
	struct watchdog_device wdt_dev;
	void __iomem *wdt_base;
};

static struct moxart_wdt_dev *moxart_wdt;
static bool nowayout = WATCHDOG_NOWAYOUT;
static int heartbeat;
static unsigned int clock_frequency;
static unsigned int max_timeout;

static int moxart_wdt_stop(struct watchdog_device *wdt_dev)
{
	struct moxart_wdt_dev *moxart_wdt = watchdog_get_drvdata(wdt_dev);
	void __iomem *wdt_base = moxart_wdt->wdt_base;

	writel(0, wdt_base + REG_ENABLE);

	return 0;
}

static int moxart_wdt_start(struct watchdog_device *wdt_dev)
{
	struct moxart_wdt_dev *moxart_wdt = watchdog_get_drvdata(wdt_dev);
	void __iomem *wdt_base = moxart_wdt->wdt_base;

	writel(clock_frequency * moxart_wdt->wdt_dev.timeout,
	       wdt_base + REG_COUNT);
	writel(0x5ab9, wdt_base + REG_MODE);
	writel(0x03, wdt_base + REG_ENABLE);

	return 0;
}

static int moxart_wdt_set_timeout(struct watchdog_device *wdt_dev,
				  unsigned int timeout)
{
	dev_dbg(wdt_dev->dev, "%s: timeout=%u\n", __func__, timeout);

	moxart_wdt->wdt_dev.timeout = timeout;

	return 0;
}

static const struct watchdog_info moxart_wdt_info = {
	.identity       = "moxart-wdt",
	.options        = WDIOF_SETTIMEOUT | WDIOF_KEEPALIVEPING |
			  WDIOF_MAGICCLOSE,
};

static const struct watchdog_ops moxart_wdt_ops = {
	.owner          = THIS_MODULE,
	.start          = moxart_wdt_start,
	.stop           = moxart_wdt_stop,
	.set_timeout    = moxart_wdt_set_timeout,
};

static int moxart_wdt_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *node = dev->of_node;
	struct resource *res;
	struct clk *clk;
	int err;

	moxart_wdt = devm_kzalloc(&pdev->dev, sizeof(*moxart_wdt), GFP_KERNEL);
	if (!moxart_wdt)
		return -EINVAL;

	platform_set_drvdata(pdev, moxart_wdt);

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	moxart_wdt->wdt_base = devm_ioremap_resource(dev, res);
	if (IS_ERR(moxart_wdt->wdt_base))
		return PTR_ERR(moxart_wdt->wdt_base);

	clk = of_clk_get(node, 0);
	if (IS_ERR(clk)) {
		pr_err("%s: of_clk_get failed\n", __func__);
		return PTR_ERR(clk);
	}

	clock_frequency = clk_get_rate(clk);

	max_timeout = UINT_MAX / clock_frequency;

	moxart_wdt->wdt_dev.info = &moxart_wdt_info;
	moxart_wdt->wdt_dev.ops = &moxart_wdt_ops;
	moxart_wdt->wdt_dev.timeout = max_timeout;
	moxart_wdt->wdt_dev.min_timeout = 1;
	moxart_wdt->wdt_dev.max_timeout = max_timeout;
	moxart_wdt->wdt_dev.parent = &pdev->dev;

	watchdog_init_timeout(&moxart_wdt->wdt_dev, heartbeat, &pdev->dev);
	watchdog_set_nowayout(&moxart_wdt->wdt_dev, nowayout);

	watchdog_set_drvdata(&moxart_wdt->wdt_dev, moxart_wdt);

	err = watchdog_register_device(&moxart_wdt->wdt_dev);
	if (unlikely(err))
		return err;

	dev_dbg(dev, "Watchdog enabled (heartbeat=%d sec, nowayout=%d)\n",
		moxart_wdt->wdt_dev.timeout, nowayout);

	return 0;
}

static int moxart_wdt_remove(struct platform_device *pdev)
{
	struct moxart_wdt_dev *moxart_wdt = platform_get_drvdata(pdev);

	moxart_wdt_stop(&moxart_wdt->wdt_dev);
	watchdog_unregister_device(&moxart_wdt->wdt_dev);

	return 0;
}

static const struct of_device_id moxart_watchdog_match[] = {
	{ .compatible = "moxa,moxart-watchdog" },
	{ },
};

static struct platform_driver moxart_wdt_driver = {
	.probe      = moxart_wdt_probe,
	.remove     = moxart_wdt_remove,
	.driver     = {
		.name		= "moxart-watchdog",
		.owner		= THIS_MODULE,
		.of_match_table	= moxart_watchdog_match,
	},
};
module_platform_driver(moxart_wdt_driver);

module_param(heartbeat, int, 0);
MODULE_PARM_DESC(heartbeat, "Watchdog heartbeat in seconds");

MODULE_DESCRIPTION("MOXART watchdog driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Jonas Jensen <jonas.jensen@gmail.com>");
