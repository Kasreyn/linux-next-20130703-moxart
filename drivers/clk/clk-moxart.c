/*
 * MOXA ART SoCs clock driver.
 *
 * Copyright (C) 2013 Jonas Jensen
 *
 * Jonas Jensen <jonas.jensen@gmail.com>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/clk-provider.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/io.h>
#include <linux/of_address.h>
#include <linux/clkdev.h>

static const struct of_device_id moxart_tclk_match[] = {
	{ .compatible = "moxa,moxart-apb-clock" },
	{ }
};

void __init moxart_of_clk_init(struct device_node *node)
{
	static void __iomem *base;
	struct device_node *clk_node;
	struct clk *clk;
	unsigned long rate;
	unsigned int mul, val, div;

	base = of_iomap(node, 0);
	if (IS_ERR(base))
		panic("%s: of_iomap failed\n", node->full_name);

	clk_node = of_find_matching_node(NULL, moxart_tclk_match);
	if (!clk_node)
		panic("%s: can't find clock DT node\n", node->full_name);

	mul = (readl(base + 0x30) >> 3) & 0x1ff;
	val = (readl(base + 0x0c) >> 4) & 0x7;

	switch (val) {
	case 1:
		div = 3;
		break;
	case 2:
		div = 4;
		break;
	case 3:
		div = 6;
		break;
	case 4:
		div = 8;
		break;
	default:
		div = 2;
		break;
	}

	/*
	 * the rate calculation below is only tested and proven
	 * to be true for UC-7112-LX
	 *
	 * UC-7112-LX: mul=80 val=0
	 *
	 * to support other moxart SoC hardware, this may need
	 * a change, though it's possible it works there too
	 */
	rate = (mul * 1200000 / div);

	clk = clk_register_fixed_rate(NULL, "clkapb", NULL, CLK_IS_ROOT, rate);
	clk_register_clkdev(clk, NULL, "clkapb");
	of_clk_add_provider(clk_node, of_clk_src_simple_get, clk);

	iounmap(base);
}
CLK_OF_DECLARE(moxart_core_clock, "moxa,moxart-core-clock", moxart_of_clk_init);
